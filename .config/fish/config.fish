# ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡀⣄⡆⡦⢔⢔⢴⡤⡀⠀⠀⠀⠀⠀⠀⠀⠀
# ⠀⠀⠀⠀⠀⠀⠀⠀⢀⣔⡪⢟⢗⢕⢅⣜⣌⣬⣼⣼⣴⣥⣀⠀⠀⠀⠀⠀⠀⠀
# ⠀⠀⠀⠀⠀⠀⠀⠀⢙⢻⡶⣕⢵⣫⣿⣷⡿⣿⠟⣿⡿⢿⣿⣿⣶⣄⠀⠀⠀⠀
# ⢀⣀⡀⠀⠀⠀⠀⠀⠿⣯⢝⣵⡟⣿⠃⡯⠀⡿⢁⣿⢁⣿⣏⠙⠿⠛⣧⡀⠀⠀
# ⢿⣿⠿⣿⣿⣶⣶⣴⣾⣿⠛⣿⠀⡿⢸⡇⢸⡇⢸⡇⢸⢿⠿⠀⠠⠀⣽⡿⣄⠀
# ⢀⣽⣭⣽⣿⡏⠟⠉⡇⢸⠀⣿⠀⠙⠭⠽⢛⣳⣽⣧⣿⡏⡆⠀⣇⠀⠉⢁⠝⡃
# ⢼⣵⠟⠉⠁⠁⠀⣴⣟⠢⠀⠘⠀⠇⢔⣒⣛⠿⡿⠟⠃⠀⠈⠢⡀⠀⠀⢀⠩⠃
# ⠈⠁⠀⠀⠀⠀⢸⣟⢿⣣⣕⠄⡀⠀⠀⢀⣀⣀⣤⣤⣤⠤⠤⠤⠴⠛⠋⠁⠀⠀
# ⠀⠀⠀⠀⠀⠀⠀⠻⣿⣿⣵⣥⠦⠏⠉⠉⠀⣠⣾⡟⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀
# ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠁⠀⠀⠀⠀⠀⠸⡛⠻⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀

# set -U fish_user_paths ~/.cargo/bin ~/.emacs.d/bin ~/.config/scripts ~/.config/emacs/bin $fish_user_paths ~/.local/bin/
source ~/.config/fish/functions/user_functions.fish

set EDITOR nvim
set VISUAL nvim
set fish_greeting

set -g fish_key_bindings fish_default_key_bindings 

source /usr/share/autojump/autojump.fish
printf %b '\e]4;4;#6495ed\a'

eval "$(thefuck --alias)"
export FZF_DEFAULT_OPTS=" --bind='ctrl-t:execute(nvim {})+abort' --cycle --reverse --prompt ' ' --pointer ' ' --marker='✔' -m --height=20% --color=bg+:#395b91,gutter:-1 --no-separator"
export MANPAGER="sh -c 'col -bx | bat --theme=OneHalfDark -l man -p'"

alias ..='cd ..'
alias ...='cd ../..'

alias vi="nvim"
alias vim="nvim -c ': NnnPicker'"
alias lv="lvim"
alias hx="flatpak run com.helix_editor.Helix -c ~/.config/helix/config.toml"
alias ra="ranger_pwd"
alias conf="ranger_pwd /home/admiral/dots"
alias pn="pnpm"
alias px="pnpx"

alias cat="bat -p --theme=OneHalfDark"
alias ls="exa --icons --group-directories-first"
alias la="exa --icons -a --group-directories-first"
alias ll="exa --icons -l --group-directories-first"
alias lp="exa | xargs sxiv -t -f"
alias tree="exa --icons --tree --group-directories-first"
alias pwd="/bin/pwd | lolcat ; /bin/pwd | xclip -selection clipboard"

alias config='/usr/bin/git --git-dir=$HOME/dots/ --work-tree=$HOME'
alias update="sudo dnf update"
alias i="sudo dnf install"
alias install="sudo dnf install"

alias nf="neofetch "
alias live="$HOME/dots/scripts/live && sleep 0.5"
alias icat="kitty +kitten icat"
alias rename_tab="zellij action rename-tab "
alias opacity='~/.config/alacritty/opacity_alacritty.sh'

bind \er 'echo " Ranger " && ranger_pwd ; xdotool key enter'
bind \eq 'exit'
bind \ee 'echo " Neovim " && nvim -c ": Telescope find_files"'
bind \eg 'echo "🚀" && cd $(pwd | find -type d -maxdepth 3  | tail -n +2 | fzf) && sleep 0.5 && xdotool key enter'
bind \eG 'echo "🚀" && cd $(echo $HOME | find -type d -maxdepth 3  | tail -n +2 | fzf ) && sleep 0.5 && xdotool key enter'
bind \ec 'echo "󰨞 Codium " && flatpak run com.vscodium.codium . ; sleep 3 ; xdotool key F11'
bind \ea 'zellij_attach' 

# bind \ef 'the-way search'

## SCRIPTS ##

if status is-interactive
    # Commands to run in interactive sessions can go here
end


