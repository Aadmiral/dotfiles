--     __                          
--    / /   __  ______  ____ ______
--   / /   / / / / __ \/ __ `/ ___/
--  / /___/ /_/ / / / / /_/ / /    
-- /_____/\__,_/_/ /_/\__,_/_/     

-- Read the docs: https://www.lunarvim.org/docs/configuration

vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
vim.opt.relativenumber = true
vim.opt.wrap = true
lvim.transparent_window = true

lvim.plugins = {
  { "catppuccin/nvim", name="catppuccin"},
  -- {'kevinhwang91/rnvimr', name="ranger"}
}

lvim.colorscheme = "catppuccin"

lvim.keys.normal_mode["<C-d>"] = "<C-d>zz"
lvim.keys.normal_mode["<Esc>"] = ":noh<CR>"
lvim.keys.normal_mode["U"] = ":redo<CR>"

lvim.keys.normal_mode["<Tab>"] = ":bnext<CR>"
lvim.keys.normal_mode["<M-l>"] = ":bnext<CR>"
lvim.keys.normal_mode["<M-h>"] = ":bprevious<CR>"
-- lvim.keys.normal_mode["<M-0>"] = false
-- lvim.keys.normal_mode["<M-1>"] = false
-- lvim.keys.normal_mode["<M-2>"] = false
-- lvim.keys.normal_mode["<M-3>"] = false
lvim.keys.normal_mode["<leader>b1"] = ":bfirst<CR>"
-- lvim.keys.normal_mode["<leader>bl"] = false
-- lvim.keys.normal_mode["<leader>tl"] = ":blast<CR>"
-- lvim.keys.normal_mode["<leader>th"] = false
-- lvim.keys.normal_mode["<leader>tl"] = ":blast<CR>"

lvim.keys.normal_mode["<leader>x"] = ":bdelete<CR>"
lvim.keys.normal_mode["<S-x>"] = ":BufferKill<CR>"

lvim.builtin.terminal.open_mapping = "<M-i>"
lvim.builtin.terminal.direction = "horizontal"
lvim.keys.normal_mode["<leader>tf"] = ":ToggleTerm<CR>"
lvim.keys.normal_mode["<leader>tt"] = ":ToggleTerm direction=horizontal<CR>"

lvim.keys.normal_mode["<leader>w"] = false
lvim.keys.normal_mode["<leader>p"] = false
lvim.keys.normal_mode["<leader>wq"] = ":wq<CR>"
lvim.keys.normal_mode["<leader>w"] = ":w<CR>"
lvim.keys.normal_mode["<leader>qq"] = ":q!<CR>"
lvim.keys.normal_mode["<M-n>"] = ":vsplit ranger<CR>"
lvim.keys.normal_mode["<M-v>"] = ":vsplit ./<CR>"

local function exit_insert_mode()
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<Esc>', true, false, true), 'n', true)
end

lvim.keys.insert_mode["jk"] = exit_insert_mode

