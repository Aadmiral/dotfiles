require("nnn").setup({
  picker = {
    cmd = "nnn ",
    session = "shared",
    style = {
      border = "rounded",
      width = 0.3,
      height = 0.5,
      xoffset = 0.5,
      yoffset = 0.5,
      fullscreen = false,
  }
  },
  explorer = {
    cmd = "nnn",
    width = 24,
    side = "topleft",
    session = "",
    tabs = true,
  },
  replace_netrw = "picker",
})

