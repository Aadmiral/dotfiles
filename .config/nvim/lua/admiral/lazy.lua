local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
  {
  'nvim-telescope/telescope.nvim', tag = '0.1.2',
  dependencies = { 'nvim-lua/plenary.nvim' }
  },

  { "catppuccin/nvim", name = "catppuccin", priority = 1000 },
  { "nvim-treesitter/nvim-treesitter"},
  {
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v2.x',
    dependencies = {
      -- LSP Support
      {'neovim/nvim-lspconfig'},
      {
        'williamboman/mason.nvim',
        build = function()
          pcall(vim.api.nvim_command, 'MasonUpdate')
        end,
      },
      {'williamboman/mason-lspconfig.nvim'},

      -- Autocompletion
      {'hrsh7th/nvim-cmp'},
      {'hrsh7th/cmp-nvim-lsp'},
      {'L3MON4D3/LuaSnip'},
    },
  },
  -- 'nvim-tree/nvim-web-devicons',
  {'tamton-aquib/staline.nvim'},
  {'lewis6991/gitsigns.nvim'},
  {
    'windwp/nvim-autopairs',
    event = "InsertEnter",
    opts = {}
  },
  {
    "NvChad/nvterm",
    config = function ()
      require("nvterm").setup()
    end,
  },
  -- {"francoiscabrol/ranger.vim"},
  {
      "luukvbaal/nnn.nvim",
      config = function() require("nnn").setup() end
  },
  -- {
  --   'goolord/alpha-nvim',
  --   event = "VimEnter",
  --   dependencies = { 'nvim-tree/nvim-web-devicons' },
  --   opts = { require'alpha.themes.dashboard'.config }
  -- }
  {"RRethy/vim-illuminate"},
  {
    'numToStr/Comment.nvim',
    config = function()
      require('Comment').setup()
    end
  },
}

local opts = {}

require("lazy").setup(plugins, opts)
