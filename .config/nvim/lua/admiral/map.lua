
vim.g.mapleader = " "
-- vim.keymap.set("n", "<leader>e", vim.cmd.Ex)
vim.keymap.set("n", "<leader>e", "<cmd>NnnPicker<CR>")
vim.keymap.set({"n","v","i","t"}, "<M-e>", "<cmd>NnnPicker<CR>")

vim.keymap.set("n", "<Esc>", "<cmd>noh<CR>")
vim.keymap.set("n", "U", "<cmd>redo<CR>")

vim.keymap.set("n", "<leader>wq", "<cmd>wq<CR>")
vim.keymap.set("n", "<leader>w", "<cmd>w<CR>")
vim.keymap.set("n", "<leader>q", "<cmd>q<CR>")
vim.keymap.set("n", "<leader>Q", "<cmd>q!<CR>")
-- vim.keymap.set("n", "<leader>/", "gcc")

vim.keymap.set("n", "<Tab>", "<cmd>bnext<CR>")
vim.keymap.set("n", "<S-Tab>", "<cmd>bprevious<CR>")
vim.keymap.set("n", "<leader>x", "<cmd>bdelete<CR>")
vim.keymap.set("n", "<S-x>", "<cmd>bdelete<CR>")

vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "J", "mzJ`z")
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

vim.keymap.set("x", "<leader>p", [["_dP]])

vim.keymap.set({"n", "v"}, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])

vim.keymap.set("n", "<leader>F", vim.lsp.buf.format)

vim.keymap.set("n", "<leader>r", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
vim.keymap.set("n", "<leader>X", "<cmd>!chmod +x %<CR>", { silent = true })

vim.keymap.set({"n","v","i","t"}, "<M-i>", function()
      require("nvterm.terminal").toggle "float"
end)

vim.keymap.set("n", "<leader>|", "<cmd>split ./<CR>")
vim.keymap.set("n", "<leader>-", "<cmd>vsplit ./<CR>")


