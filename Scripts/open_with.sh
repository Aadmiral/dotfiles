#!/bin/bash

declare -A options
options["Foliate"]="flatpak run com.github.johnfactotum.Foliate"
options["Xournal++"]="flatpak run com.github.xournalpp.xournalpp"
options["nvim"]="vim w"

user_options=${!options[@]}
user_options=$(echo "$user_options" | tr ' ' '\n')

user_choice=$(printf "$user_options" | gum choose --ordered --selected="Clone" --cursor=" ")
open_with=${options[$user_choice]}

echo $open_with
$open_with $1
